from frappe import _

def get_data():
	return [
		{
			"module_name": "Hiboutik",
			"color": "orange",
			"icon": "octicon octicon-plug",
			"type": "module",
			"label": _("Hiboutik")
		}
	]
