import frappe

from hiboutik.controllers.item import sync_all_items as _sync_all_items
from hiboutik.hiboutik.doctype.hiboutik_till_movement.hiboutik_till_movement import hourly_check

def sync_all_items():
	if frappe.db.get_single_value("Hiboutik Settings", "enable_sync"):
		return _sync_all_items()

def hourly_till_check():
	return hourly_check()
