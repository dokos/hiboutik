from . import __version__ as app_version  # noqa

app_name = "hiboutik"
app_title = "Hiboutik"
app_publisher = "Dokos SAS"
app_description = "Hiboutik connector for Dokos/ERPNext"
app_icon = "octicon octicon-plug"
app_color = "orange"
app_email = "hello@dokos.io"
app_license = "GPLv3"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/hiboutik/css/hiboutik.css"
# app_include_js = "/assets/hiboutik/js/hiboutik.js"

# include js, css files in header of web template
# web_include_css = "/assets/hiboutik/css/hiboutik.css"
# web_include_js = "/assets/hiboutik/js/hiboutik.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "hiboutik/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {"Item": "public/js/hiboutik_sync.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
# 	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------
# add methods and filters to jinja environment
# jinja = {
# 	"methods": "hiboutik.utils.jinja_methods",
# 	"filters": "hiboutik.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "hiboutik.install.before_install"
# after_install = "hiboutik.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "hiboutik.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes
# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Customer": {"on_update": "hiboutik.controllers.customer.on_update"},
	"Address": {"on_update": "hiboutik.controllers.address.on_update"},
	"Bin": {"on_update": "hiboutik.controllers.stock.after_bin_update"},
	"Journal Entry": {"on_submit": "hiboutik.controllers.journal_entry.on_submit"},
	"Item": {"on_update": "hiboutik.controllers.item.on_update"},
}

# Scheduled Tasks
# ---------------

scheduler_events = {
	"hourly": [
		"hiboutik.controllers.sales_invoice.check_closed_sales",
		"hiboutik.tasks.hourly_till_check"
	],
	"daily_long": [
		"hiboutik.tasks.sync_all_items"
	]
}

# Testing
# -------

before_tests = "hiboutik.tests.utils.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "hiboutik.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "hiboutik.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

# User Data Protection
# --------------------
# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------
# auth_hooks = [
# 	"hiboutik.auth.validate"
# ]
