# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.utils import flt
from frappe.model.document import Document

from hiboutik.hiboutik.api import HiboutikAPI

from erpnext.accounts.doctype.journal_entry.journal_entry import get_default_bank_cash_account

class HiboutikTillMovement(Document):
	pass


def hourly_check():
	if frappe.db.get_single_value("Hiboutik Settings", "enable_sync"):
		hiboutik_settings = frappe.get_single("Hiboutik Settings")
		hiboutik_api = HiboutikAPI(hiboutik_settings)

		movements = hiboutik_api.get_till_movements()
		for movement in movements:
			if not frappe.db.exists("Hiboutik Till Movement", movement.get("till_id")):
				doc = frappe.new_doc("Hiboutik Till Movement")
				doc.update(movement)
				doc.insert(ignore_permissions=True)

@frappe.whitelist()
def make_journal_entry(name):
	till = frappe.get_doc("Hiboutik Till Movement", name)
	pos_profile = frappe.db.get_single_value("Hiboutik Settings", "pos_profile")
	company = frappe.db.get_value("POS Profile", pos_profile, "company")

	cash_account_data = get_default_bank_cash_account(company, "Cash")
	bank_account_data = get_default_bank_cash_account(company, "Bank")
	transfer_account = frappe.db.get_value("Company", company, "inter_banks_transfer_account")

	cash_debit = cash_credit = bank_debit = bank_credit = 0.0

	jv = frappe.new_doc("Journal Entry")
	jv.posting_date = till.date_till
	jv.hiboutik_till_movement = name

	if flt(till.withdrawal) > 0:
		cash_credit = bank_debit = flt(till.withdrawal)

	elif flt(till.deposit) > 0:
		cash_debit = bank_credit = abs(flt(till.deposit))

	elif flt(till.balance) < 0:
		cash_credit = bank_debit = abs(flt(till.balance))

	elif flt(till.balance) > 0:
		cash_debit = bank_credit = abs(flt(till.balance))

	if cash_account_data:
		jv.append("accounts", {
			"account": cash_account_data.account,
			"debit_in_account_currency": cash_debit,
			"credit_in_account_currency": cash_credit
		})

	if transfer_account:
		jv.append("accounts", {
			"account": transfer_account,
			"debit_in_account_currency": bank_debit,
			"credit_in_account_currency": bank_credit
		})

		jv.append("accounts", {
			"account": transfer_account,
			"debit_in_account_currency": cash_debit,
			"credit_in_account_currency": cash_credit
		})

	if bank_account_data:
		jv.append("accounts", {
			"account": bank_account_data.account,
			"debit_in_account_currency": bank_debit,
			"credit_in_account_currency": bank_credit
		})

	return jv.as_dict()