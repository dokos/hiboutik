frappe.listview_settings['Hiboutik Sales'] = {
	get_indicator: function (doc) {
		if (doc.status === "Pending") {
			return [__("Pending"), "orange", "status,=,Pending"];
		} else if (doc.status === "Completed") {
			return [__("Completed"), "green", "status,=,Completed"];
		} else if (doc.status === "Error") {
			return [__("Error"), "red", "status,=,Error"];
		}
	}
}