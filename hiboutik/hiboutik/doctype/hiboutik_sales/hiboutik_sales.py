# Copyright (c) 2022, ioCraft, Dokos and contributors
# For license information, please see license.txt

from datetime import datetime

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import cint, flt, getdate

from hiboutik.hiboutik.api import HiboutikAPI
from hiboutik.hiboutik.models import (
	HiboutikPaymentData,
	HiboutikSalesInvoice,
	HiboutikSalesInvoiceItem,
	HiboutikTaxData,
)


class HiboutikSales(Document):
	def after_insert(self):
		customer = frappe.parse_json(self.data).get("customer_id")
		if customer != "0":
			frappe.enqueue(
				"hiboutik.webhooks.create_update_customer", customer_id=customer
			)

		frappe.enqueue_doc(
			self.doctype, self.name, "create_sales_invoice", now=frappe.flags.in_test
		)

	@frappe.whitelist()
	def create_sales_invoice(self):
		hiboutik_settings = frappe.get_single("Hiboutik Settings")
		hiboutik_api = HiboutikAPI(hiboutik_settings)

		sales = hiboutik_api.get_sales(frappe.parse_json(self.data).get("sale_id"))
		for sale in sales:
			if sale.get("comments", "").startswith("inventory"):
				return self.db_set("status", "Completed")

			self.sales_data = sale
			sales_invoice_data = self.convert_payload_to_sales_invoice()

			if self.error:
				self.db_set("error", None)
			try:
				self.make_sales_invoice(sales_invoice_data)
				self.db_set("status", "Completed")
			except Exception as e:
				self.db_set("error", str(e))
				self.db_set("status", "Error")

	def convert_payload_to_sales_invoice(self) -> HiboutikSalesInvoice:
		# TODO: timezone not available in Hiboutik data.
		# We assume that Dokos and Hiboutik timezones are aligned for now.
		# Can be subject to a future enhancement.
		return HiboutikSalesInvoice(
			posting_date=getdate(
				datetime.fromisoformat(self.sales_data["completed_at"])
			),
			invoice_items=self.get_invoice_item_details(),
			customer=self._get_customer(),
			pos_profile=self._get_pos_profile(),
			company=self._get_company(),
			currency=self.sales_data["currency"],
			external_id=self.sales_data["unique_sale_id"],
			payment_method=self.sales_data["payment"],
			payment_items=self.get_payment_details(),
			total=self.sales_data["total"],
			taxes=self.get_taxes(),
		)

	def get_invoice_item_details(self):
		invoice_items = []

		for item_data in self.sales_data.get("line_items", []):
			invoice_items.append(
				HiboutikSalesInvoiceItem(
					qty=int(item_data["quantity"])
					* (-1 if flt(item_data["product_price"]) < 0 else 1),
					external_id=item_data["product_id"],
					rate=abs(
						flt(item_data["item_unit_gross"])
						if item_data["discount"]
						else flt(item_data["item_unit_gross_wo_discount"])
					),
					price_list_rate=abs(flt(item_data["item_unit_gross_wo_discount"])),
					amount=abs(flt(item_data["item_total_net"])),
					tax_template=get_tax_template(item_data["tax_value"]),
					tax_included=item_data["item_total_net"]
					!= item_data["item_total_gross"],
					description=item_data["product_model"],
					discount=abs(flt(item_data["item_unit_discount_gross"])),
					discount_percentage=abs(
						cint(item_data["item_discount_percentage_round_0"])
					),
				)
			)

		return invoice_items

	def get_payment_details(self):
		payment_items = []

		for payment_data in self.sales_data.get("payment_details", []):
			if payment_data["payment_type"] != "CRED":
				payment_items.append(
					HiboutikPaymentData(
						payment_type=self._get_payment_method(
							payment_data["payment_type"]
						),
						payment_amount=payment_data["payment_amount"],
					)
				)

		return payment_items

	def get_taxes(self):
		taxes = []

		for tax_data in self.sales_data.get("taxes", []):
			taxes.append(
				HiboutikTaxData(
					tax_value=flt(tax_data["tax_value"]) * 100.0,
					tax_description=tax_data["tax_label"],
					tax_total=tax_data["total_vat"],
				)
			)

		return taxes

	def _get_customer(self):
		customer = frappe.db.get_value(
			"Customer", dict(hiboutik_id=self.sales_data["customer_id"])
		)
		if not customer:
			pos_profile = self._get_pos_profile()
			customer = frappe.db.get_value("POS Profile", pos_profile, "customer")

		return customer

	def _get_company(self):
		pos_profile = self._get_pos_profile()
		return frappe.db.get_value("POS Profile", pos_profile, "company")

	@staticmethod
	def _get_pos_profile():
		return frappe.db.get_single_value("Hiboutik Settings", "pos_profile")

	@staticmethod
	def _get_payment_method(payment_type: str):
		return frappe.db.get_value(
			"Hiboutik Payment Methods",
			dict(hiboutik_payment_method=payment_type),
			"mode_of_payment",
		)

	def make_sales_invoice(self, sales_invoice_data: HiboutikSalesInvoice):
		"""Create a POS Invoice in the ERP from the external POS data"""
		if frappe.db.exists(
			"Sales Invoice",
			dict(hiboutik_id=sales_invoice_data.external_id, docstatus=("!=", 2)),
		):
			return

		if not flt(sales_invoice_data.total):
			return

		sinv_doc = frappe.new_doc("Sales Invoice")
		sinv_doc.flags.ignore_permissions = True

		sinv_doc.hiboutik_id = sales_invoice_data.external_id
		sinv_doc.hiboutik_sales = self.name
		sinv_doc.customer = sales_invoice_data.customer
		sinv_doc.pos_profile = sales_invoice_data.pos_profile
		sinv_doc.company = sales_invoice_data.company
		sinv_doc.update_stock = 1
		sinv_doc.is_pos = 1
		sinv_doc.set_posting_time = 1
		sinv_doc.posting_date = sales_invoice_data.posting_date
		sinv_doc.currency = sales_invoice_data.currency
		sinv_doc.ignore_pricing_rule = 1
		sinv_doc.is_return = flt(sales_invoice_data.total) < 0.0
		sinv_doc.payments = []

		for item in sales_invoice_data.invoice_items:
			# The rule for tax inclusion should be the same for all items
			item_code = (
				get_item_code_from_external_id(item.external_id)
				if cint(item.external_id) != 0
				else (
					frappe.db.get_single_value("Hiboutik Settings", "generic_return_item")
					if sinv_doc.is_return
					else None
				)
			)
			if not item_code:
				frappe.throw(
					_("Hiboutik item n°{0} cannot be found").format(item.external_id)
				)

			sinv_doc.append(
				"items",
				{
					"item_code": item_code,
					"qty": item.qty,
					"rate": item.rate,
					"price_list_rate": item.price_list_rate,
					"item_tax_template": item.tax_template,
					"discount_amount": 100.0 if item.rate == 0.0 else item.discount,
					"discount_percentage": item.discount_percentage,
				},
			)

		if sales_invoice_data.payment_method == "CRED":
			sinv_doc.is_pos = 0

		elif sales_invoice_data.payment_items:
			payment_total = 0.0
			for pay in sales_invoice_data.payment_items:
				paid_amount = (
					pay.payment_amount
					if (payment_total + flt(pay.payment_amount))
					<= flt(sales_invoice_data.total)
					else (flt(sales_invoice_data.total) - payment_total)
				)
				sinv_doc.append(
					"payments",
					{"mode_of_payment": pay.payment_type, "amount": paid_amount},
				)

				# Register a different payment for repaid customer credits to allow reconciliation
				payment_diff = flt(pay.payment_amount) - flt(paid_amount)
				if payment_diff > 0:
					make_payment_entry(sinv_doc, payment_diff, pay.payment_type)

				payment_total += flt(pay.payment_amount)

		elif sales_invoice_data.payment_method != "CRED":
			sinv_doc.append(
				"payments",
				{
					"mode_of_payment": self._get_payment_method(
						sales_invoice_data.payment_method
					),
					"amount": sales_invoice_data.total,
				},
			)

		sinv_doc.set_missing_values(for_validate=True)

		sinv_doc.taxes = []
		for item in sales_invoice_data.invoice_items:
			if cint(item.external_id) == 0 and not sinv_doc.is_return:
				add_global_discounts(item, sinv_doc)

		add_taxes(sales_invoice_data, sinv_doc)

		sinv_doc.calculate_taxes_and_totals()
		if flt(sinv_doc.rounded_total) != flt(sales_invoice_data.total):
			frappe.throw(
				_("Sales invoice grand total and hiboutik sales total are not equal")
			)

		sinv_doc.insert()
		sinv_draft = frappe.db.get_single_value("Hiboutik Settings", "nominative_invoices_draft")

		if (not sinv_draft) or \
			(sinv_doc.customer == frappe.db.get_value("POS Profile", sinv_doc.pos_profile, "customer")):
			sinv_doc.submit()

		return sinv_doc


def get_tax_template(hiboutik_tax_value):
	return frappe.db.get_value(
		"Hiboutik VAT Rates",
		dict(hiboutik_rate=flt(hiboutik_tax_value)),
		"item_tax_template",
	)


def get_item_code_from_external_id(external_id: str):
	"""Given the Hiboutik stored external ID, retrieve the Dokos Item code"""
	return frappe.db.get_value("Item", dict(hiboutik_id=external_id))


def add_global_discounts(item, sinv):
	cost_center = frappe.db.get_value(
		"POS Profile", sinv.pos_profile, "cost_center"
	) or frappe.db.get_value("Company", sinv.company, "cost_center")
	discount_account = frappe.db.get_single_value(
		"Hiboutik Settings", "discount_account"
	)
	sinv.append(
		"taxes",
		{
			"charge_type": "Actual",
			"account_head": discount_account,
			"tax_amount": flt(item.amount),
			"description": flt(item.description),
			"cost_center": cost_center,
		},
	)


def add_taxes(sales_invoice_data, sinv):
	cost_center = frappe.db.get_value(
		"POS Profile", sinv.pos_profile, "cost_center"
	) or frappe.db.get_value("Company", sinv.company, "cost_center")
	for tax in sales_invoice_data.taxes:
		tax_template = get_tax_template(tax.tax_value)
		if tax_template:
			tax_template_doc = frappe.get_doc("Item Tax Template", tax_template)
			sinv.append(
				"taxes",
				{
					"charge_type": "On Net Total",
					"account_head": tax_template_doc.taxes[0].tax_type,
					"tax_amount": flt(tax.tax_total),
					"description": tax.tax_description,
					"cost_center": cost_center,
					"included_in_print_rate": 1,
				},
			)


def make_payment_entry(sales_invoice_data, amount, payment_type):
	mode_of_payment = frappe.db.get_value(
		"Hiboutik Payment Methods",
		dict(hiboutik_payment_method=payment_type),
		"mode_of_payment",
	)
	bank_account = get_bank_cash_account(sales_invoice_data.company, mode_of_payment)

	pe = frappe.get_doc(
		{
			"doctype": "Payment Entry",
			"company": sales_invoice_data.company,
			"currency": sales_invoice_data.currency,
			"payment_type": "Receive",
			"party_type": "Customer",
			"party": sales_invoice_data.customer,
			"paid_to": bank_account.get("account"),
			"mode_of_payment": mode_of_payment,
			"paid_amount": abs(flt(amount)),
			"received_amount": abs(flt(amount)),
			"reference_no": _("Hiboutik Credit from Sales N°{0}").format(
				sales_invoice_data.hiboutik_sales
			),
			"reference_date": sales_invoice_data.posting_date,
		}
	)

	pe.set_missing_values()

	pe.insert(ignore_permissions=True)
	return pe.submit()


def get_bank_cash_account(company, mode_of_payment):
	from erpnext.accounts.doctype.journal_entry.journal_entry import (
		get_default_bank_cash_account,
	)

	bank = get_default_bank_cash_account(company, "Bank", mode_of_payment)

	if not bank:
		bank = get_default_bank_cash_account(company, "Cash", mode_of_payment)

	return bank
