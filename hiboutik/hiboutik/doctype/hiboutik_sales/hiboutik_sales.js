// Copyright (c) 2022, ioCraft, Dokos and contributors
// For license information, please see license.txt

frappe.ui.form.on('Hiboutik Sales', {
	refresh: function(frm) {
		if (["Error", "Pending"].includes(frm.doc.status)) {
			frm.add_custom_button(__("Retry"), () =>
				frappe.call({
					doc: frm.doc,
					method: 'create_sales_invoice',
				}).then(() => {
					frappe.show_alert({
						message: __("Retry in progress"),
						indicator: "green"
					})
					frm.reload_doc()
				})
			);
		}
	}
});
