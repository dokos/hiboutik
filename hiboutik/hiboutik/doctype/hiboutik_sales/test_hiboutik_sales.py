# Copyright (c) 2022, ioCraft, Dokos and contributors
# See license.txt

import unittest

import frappe
from frappe.utils import flt

from hiboutik.webhooks import pos_invoice_webhook

class TestHiboutikSales(unittest.TestCase):
	def setUp(self) -> None:
		pass

	def test_simple_sale(self):
		with open(frappe.get_app_path("hiboutik", "hiboutik", "doctype", "hiboutik_sales", "data", "simple_sale.json")) as f:
			content = frappe.parse_json(f.read())
			self.check_status_and_total(content)

	def test_sale_with_discount(self):
		with open(frappe.get_app_path("hiboutik", "hiboutik", "doctype", "hiboutik_sales", "data", "sale_with_discount.json")) as f:
			content = frappe.parse_json(f.read())
			self.check_status_and_total(content)

	def test_sale_with_multiple_discounts(self):
		with open(frappe.get_app_path("hiboutik", "hiboutik", "doctype", "hiboutik_sales", "data", "sale_with_multiple_discounts.json")) as f:
			content = frappe.parse_json(f.read())
			self.check_status_and_total(content)

	def check_status_and_total(self, content):
		sales = pos_invoice_webhook(**content)

		sales.load_from_db()
		self.longMessage = True
		self.assertEqual(sales.status, "Completed", sales.error)

		sinv = frappe.get_all("Sales Invoice", filters={"hiboutik_sales": sales.name}, fields=["grand_total"])

		self.assertEqual(sinv[0].grand_total, flt(content.get("total")))
