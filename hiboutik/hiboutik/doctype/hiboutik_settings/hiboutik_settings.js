// Copyright (c) 2021, ioCraft, Dokos and contributors
// For license information, please see license.txt

frappe.ui.form.on('Hiboutik Settings', {
	onload: function (frm) {
		frm.set_query('generic_return_item', function(doc) {
			return {
				filters: {
					"is_stock_item": 0
				}
			};
		});
	},
	refresh: function (frm) {
		if (frm.doc.enable_sync) {
			frm.add_custom_button(__("Sync all items"), function () {
				frappe.call({
					type: "POST",
					method: "hiboutik.controllers.item.sync_all_items",
				}).done(() => {
					frappe.show_alert({
						indicator: "green",
						message: __("All items synchronized with Hiboutik")
					});
				}).fail(() => {
					frappe.show_alert({
						indicator: "red",
						message: __("Items failed to synchronize with Hiboutik")
					});
				});
			}, __("Actions"));


			frm.add_custom_button(__("Sync all customers"), function () {
				frappe.show_alert({
					indicator: "green",
					message: __("Synchronization in progress")
				});
				frappe.call({
					type: "POST",
					method: "hiboutik.controllers.customer.sync_all_customers",
				}).done(() => {
					frappe.show_alert({
						indicator: "green",
						message: __("Customers synchronized with Hiboutik")
					});
				}).fail(() => {
					frappe.show_alert({
						indicator: "red",
						message: __("Customers failed to synchronize with Hiboutik")
					});
				});
			}, __("Actions"));

			frm.dashboard.clear_headline();
			frm.dashboard.set_headline_alert(`
				${__("If you need a different naming series for your invoices, you can create a")} <a href="/app/document-naming-rule">${__("Document Naming Rule")}</a>
			`);
		}
	}
});
