from typing import List

import requests
from requests.auth import HTTPBasicAuth

import frappe
from frappe import _
from frappe.utils import getdate, flt, cint
from hiboutik.hiboutik.models import (
	Item, Customer, ProductData, SyncedItem, ProductAttribute, Product, Webhook,
	InventoryInputData, InventoryInputDetailData, Tax,
	SALE_WEBHOOK_APP_ID, HIBOUTIK_DEFAULT_STOCK_ID, HIBOUTIK_DEFAULT_PRODUCT_SIZE
)

class HiboutikStoreError(Exception):
	pass

class HiboutikAPIError(Exception):
	pass

class HiboutikAPIInsufficientRightsError(Exception):
	pass

class HiboutikConnector:
	def __init__(self, api):
		self.api = api

	def sync(self, item: Item):
		if item.external_id:
			# FIXME: handle partial update due to some failures (some calls succeed, some don't).
			try:
				existing_product = self.api.get_product(item.external_id)
			except HiboutikAPIError as e:
				if frappe.parse_json(e.args[0]).get("error") == "not_found":
					frappe.db.set_value("Item", item.code, "hiboutik_id", None)
					item.external_id = None
					return self.sync(item)

			update = []
			existing_data = existing_product.data

			for k, v in ProductData.create(item).data.items():
				if existing_data[k] != v:
					update.append(ProductAttribute(k, str(v)))
			self.api.update_product(item.external_id, update)
			synced_item = SyncedItem(
				item, existing_product.product_id, existing_product
			)
		else:
			item.external_id = str(self.api.post_product(Product.create(item)))
			synced_item = SyncedItem(item, int(item.external_id))

		StockSyncer(self.api).sync(synced_item)

		return item

	def delete_webhooks(self):
		webhooks = self.api.get_webhooks()
		for webhook in webhooks:
			if webhook.webhook_app_id_int == SALE_WEBHOOK_APP_ID:
				self.api.delete_webhook(webhook.webhook_id)

	def set_webhook(self, webhook: Webhook):
		self.api.post_webhook(webhook.data)


class StockSyncer:
	def __init__(self, api):
		self.api = api

	def sync(self, item: SyncedItem):
		if not self.api.settings.stock_synchronization or not item.is_stock_item or not item.product:
			return

		default_stock = [stock for stock in item.product.stock_available if stock.warehouse_id==HIBOUTIK_DEFAULT_STOCK_ID]
		pos_stock = default_stock[0].stock_available if default_stock else 0

		if item.stock_qty != pos_stock:
			self.api.post_inventory_count(
				product_id=item.external_id,
				quantity=max(0.0, flt(item.stock_qty))
			)

			self.api.close_inventory_count()

		default_language = frappe.db.get_single_value("System Settings", "language")
		if self.api.settings.stock_alert and default_stock and cint(item.stock_qty) <= default_stock[0].stock_alert:
			self.api.send_message(
				_("Stock level alert", lang=default_language),
				_("Low stock levels for {0}: {1} left", lang=default_language).format(item.name, cint(item.stock_qty))
			)

class HiboutikAPI:
	def __init__(self, settings):
		self.settings = settings
		self.account = settings.account
		self.host = f"{settings.account}.hiboutik.com"
		self.user = settings.api_login
		self.api_key = settings.get_password(fieldname="api_key")

		self.headers = {"Accept": "application/json"}

		self.session = requests.Session()
		self.session.headers.update(self.headers)

		self.api_root = "https://{0}/api".format(self.host)

	def _log(func):
		def logcall(self, *args, **kwargs):
			if frappe.conf.log_hiboutik_api:
				frappe.get_doc({
					"doctype": "Hiboutik API Log",
					"api_call": func.__name__
				}).insert(ignore_permissions=True)
			return func(self, *args, **kwargs)
		return logcall

	@_log
	def get_products(self) -> List[Product]:
		response = self.session.get(
			f"{self.api_root}/products",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get products>{response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return list(map(lambda p: Product.create_from_data(p), response.json()))

	@_log
	def post_product(self, product: Product) -> int:
		response = self.session.post(
			f"{self.api_root}/products",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=product.data,
		)
		# print(f"HIBOUTIK post product>{response.text}")
		if response.status_code != 201:
			raise HiboutikAPIError(response.json())
		return response.json()["product_id"]

	@_log
	def _put_product(self, product_id: int, data):
		response = self.session.put(
			f"{self.api_root}/product/{product_id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK put product {product_id} {data}>{response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())

	@_log
	def get_product(self, product_id: int):
		response = self.session.get(
			f"{self.api_root}/products/{product_id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get product {product_id}>{response.text}")
		if response.status_code == 200:
			return Product.create_from_data(response.json()[0])
		else:
			raise HiboutikAPIError(response.json())

	def update_product(self, product_id: int, update: List[ProductAttribute]):
		for pa in update:
			self._put_product(product_id, pa.__dict__)

	@_log
	def get_webhooks(self) -> List[Webhook]:
		response = self.session.get(
			f"{self.api_root}/webhooks",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get webhooks > {response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return list(map(lambda i: Webhook.create_from_data(i), response.json()))

	@_log
	def post_webhook(self, data: dict) -> int:
		response = self.session.post(
			f"{self.api_root}/webhooks",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK post webhook\n{data}\n>\n{response.text}")
		if response.status_code == 200:
			return response.json()["webhook_id"]
		elif response.status_code == 403:
			raise HiboutikAPIInsufficientRightsError(response.json())
		else:
			raise HiboutikAPIError(response.json())

	@_log
	def delete_webhook(self, webhook_id: int):
		response = self.session.delete(
			f"{self.api_root}/webhooks/{webhook_id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK delete webhook {webhook_id} >\n{response.text}")
		if response.status_code == 403:
			raise HiboutikAPIInsufficientRightsError(response.json())
		elif response.status_code != 200:
			raise HiboutikAPIError(response.json())

	@_log
	def post_inventory_count(self, product_id: int, quantity: int):
		response = self.session.post(
			f"{self.api_root}/inventory_counts",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=dict(
				product_id=product_id,
				warehouse_id=HIBOUTIK_DEFAULT_STOCK_ID,
				quantity=quantity
			),
		)
		# print(f"HIBOUTIK post inventory count >\n{response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def close_inventory_count(self):
		response = self.session.post(
			f"{self.api_root}/inventory_close",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=dict(
				warehouse_id=HIBOUTIK_DEFAULT_STOCK_ID,
			),
		)
		# print(f"HIBOUTIK post inventory count >\n{response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def post_inventory_input_for_product(self, product_id: int, quantity: int):
		inv_input_id = self.post_inventory_input(
			InventoryInputData(HIBOUTIK_DEFAULT_STOCK_ID)
		)
		self.post_inventory_input_details(
			inv_input_id,
			InventoryInputDetailData(
				quantity, product_id, HIBOUTIK_DEFAULT_PRODUCT_SIZE
			),
		)
		self.validate_inventory_input(inv_input_id)

	@_log
	def post_inventory_input(self, inv_input: InventoryInputData) -> int:
		data = inv_input.__dict__.copy()
		response = self.session.post(
			f"{self.api_root}/inventory_inputs",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK post inventory input\n{data}\n>\n{response.text}")
		if response.status_code == 201:
			return response.json()["inventory_input_id"]
		else:
			raise HiboutikAPIError(response.json())

	@_log
	def post_inventory_input_details(
		self, inv_input_id: int, inv_input_detail: InventoryInputDetailData
	) -> int:
		data = inv_input_detail.__dict__.copy()
		response = self.session.post(
			f"{self.api_root}/inventory_input_details/{inv_input_id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(
		# 	f"HIBOUTIK post inventory input detail\n{data}\n>\n{response.text}"
		# )
		if response.status_code == 201:
			return response.json()["inventory_input_detail_id"]
		else:
			raise HiboutikAPIError(response.json())

	@_log
	def validate_inventory_input(self, inventory_input_id: int):
		data = {"inventory_input_id": inventory_input_id}
		response = self.session.post(
			f"{self.api_root}/inventory_input_validate",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(
		# 	f"HIBOUTIK post inventory input validate\n{data}\n>\n{response.text}"
		# )
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())

	@_log
	def get_taxes(self) -> List[Tax]:
		response = self.session.get(
			f"{self.api_root}/taxes",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get taxes > {response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return list(map(lambda i: Tax.create_from_data(i), response.json()))

	@_log
	def get_customer(self, id) -> List[Customer]:
		response = self.session.get(
			f"{self.api_root}/customer/{id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get customer {id} > {response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return list(map(lambda i: Customer.create_from_data(i), response.json()))

	@_log
	def post_customer(self, data):
		response = self.session.post(
			f"{self.api_root}/customers",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK post customer > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def put_customer(self, id, data):
		response = self.session.put(
			f"{self.api_root}/customer/{id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK put customer {id} > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def post_customer_address(self, customers_id):
		response = self.session.post(
			f"{self.api_root}/customers_addresses",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data={
				"customers_id": customers_id
			},
		)
		# print(f"HIBOUTIK post customer address > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def put_customer_address(self, id, data):
		response = self.session.put(
			f"{self.api_root}/customers_addresses/{id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data=data,
		)
		# print(f"HIBOUTIK put customer address {id} > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def get_z_by_payment_received(self, store_id=HIBOUTIK_DEFAULT_STOCK_ID, date=getdate()):
		response = self.session.get(
			f"{self.api_root}/z/payment_received/{store_id}/{date.year}/{date.month}/{date.day}",
			auth=HTTPBasicAuth(self.user, self.api_key)
		)
		# print(f"HIBOUTIK get z by payment received for {date} > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def get_till_movements(self, store_id=HIBOUTIK_DEFAULT_STOCK_ID, date=getdate()):
		response = self.session.get(
			f"{self.api_root}/till/{store_id}/{date.year}/{date.month}",
			auth=HTTPBasicAuth(self.user, self.api_key)
		)
		# print(f"HIBOUTIK get till movements > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def get_closed_sales(self, store_id=HIBOUTIK_DEFAULT_STOCK_ID, date=getdate()):
		response = self.session.get(
			f"{self.api_root}/closed_sales/{store_id}/{date.year}/{date.month}/{date.day}",
			auth=HTTPBasicAuth(self.user, self.api_key)
		)
		# print(f"HIBOUTIK get closed sales > {response.text}")
		if response.status_code not in (200, 201):
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def get_sales(self, id) -> List[Customer]:
		response = self.session.get(
			f"{self.api_root}/sales/{id}",
			auth=HTTPBasicAuth(self.user, self.api_key),
		)
		# print(f"HIBOUTIK get sales {id} > {response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return response.json()

	@_log
	def send_message(self, title, message, message_object="none", object_id=None):
		response = self.session.post(
			f"{self.api_root}/message",
			auth=HTTPBasicAuth(self.user, self.api_key),
			data={
				"store_id": HIBOUTIK_DEFAULT_STOCK_ID,
				"thread": "notifications",
				"object": message_object,
				"object_id": object_id,
				"title": title,
				"message": message
			}
		)
		# print(f"HIBOUTIK post message > {response.text}")
		if response.status_code != 200:
			raise HiboutikAPIError(response.json())
		return response.json()
