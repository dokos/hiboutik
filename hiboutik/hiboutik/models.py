import datetime
from dataclasses import dataclass
from typing import List

from hiboutik.utils.data import get_country_name

#: Identifies uniquely the sale webhook for Hiboutik.
#: Allows to update with DELETE+POST if it already exists.
SALE_WEBHOOK_APP_ID = "DOKOS"

HIBOUTIK_DEFAULT_STOCK_ID = 1
HIBOUTIK_DEFAULT_PRODUCT_SIZE = 0

@dataclass
class Item:
	"""A proxy Item to hide Dokos internals"""
	code: str
	name: str
	price: str
	vat: int
	is_stock_item: bool
	deactivated: bool = False
	external_id: str = ""
	stock_qty: int = 0

@dataclass
class HiboutikSalesInvoiceItem:
	"""A sell item on a POS Invoice"""
	qty: int
	external_id: str
	tax_template: str
	tax_included: bool
	description: str
	code: str = ""
	rate: float = 0.0
	price_list_rate: float = 0.0
	amount: float = 0.0
	discount: float = 0.0
	discount_percentage: float = 0.0

@dataclass
class HiboutikPaymentData:
	"""Payments data received from an external POS"""
	payment_type: str
	payment_amount: float

@dataclass
class HiboutikTaxData:
	"""Tax data received from an external POS"""

	tax_value: float
	tax_description: str
	tax_total: float = 0.0

@dataclass
class HiboutikSalesInvoice:
	"""An invoice received from an external POS"""
	company: str
	posting_date: datetime.datetime
	customer: str
	pos_profile: str
	external_id: str
	currency: str
	payment_method: str
	total: float
	invoice_items: List[HiboutikSalesInvoiceItem]
	payment_items: List[HiboutikPaymentData]
	taxes: List[HiboutikTaxData]

@dataclass
class Address:
	"""An address received from an external POS"""
	external_id: str
	address: str
	zip_code: str
	city: str
	country: str

@dataclass
class Customer:
	"""A customer received from an external POS"""
	external_id: str
	first_name: str
	last_name: str
	company: str
	email: str
	phone: str
	addresses: List[Address]

	@classmethod
	def create_from_data(cls, data: dict):
		addresses = [
			Address(
				external_id=add.get("address_id"),
				address=add.get("address"),
				zip_code=add.get("zip_code"),
				city=add.get("city"),
				country=get_country_name(add.get("country") or 'FRA'),
			) for add in data.get("addresses", [])
		]
		return Customer(
			external_id=data.get("customers_id"),
			first_name=data.get("first_name"),
			last_name=data.get("last_name"),
			company=data.get("company"),
			email=data.get("email"),
			phone=data.get("phone"),
			addresses=addresses,
		)

@dataclass
class ProductData:
	product_model: str
	product_price: str
	product_vat: int
	product_arch: int  # Is the product archived, 0 for no, 1 for yes.
	product_stock_management: int  # Is the product stock-managed, 0 for no, 1 for yes.
	products_ref_ext: str

	@classmethod
	def create(cls, item: Item):
		return ProductData(
			product_model=item.name,
			product_price=str(item.price),
			product_vat=item.vat,
			product_arch=int(item.deactivated),
			product_stock_management=int(item.is_stock_item),
			products_ref_ext=item.code
		)

	@property
	def data(self) -> dict:
		return self.__dict__.copy()


@dataclass
class ProductStock:
	stock_available: int
	stock_alert: int
	warehouse_id: int

	@classmethod
	def create_from_data(cls, data: dict):
		return ProductStock(
			stock_available=data["stock_available"],
			stock_alert=data["inventory_alert"],
			warehouse_id=data["warehouse_id"]
		)


@dataclass
class Product(ProductData):
	product_id: int
	stock_available: List[ProductStock]

	@classmethod
	def create_from_data(cls, data: dict):
		return Product(
			product_id=data["product_id"],
			product_model=data["product_model"],
			product_price=data["product_price"],
			product_vat=data["product_vat"],
			product_arch=data["product_arch"],
			product_stock_management=data["product_stock_management"],
			products_ref_ext=data["products_ref_ext"],
			stock_available=[
				ProductStock.create_from_data(st) for st in data["stock_available"]
			],
		)

	@property
	def data(self) -> dict:
		rv = self.__dict__.copy()
		del rv["stock_available"]
		del rv["product_id"]
		return rv


@dataclass
class ProductAttribute:
	product_attribute: str
	new_value: str


@dataclass
class SyncedItem(Item):
	product: Product or None = None

	def __init__(self, item: Item, product_id: int, product: Product = None):
		item_data = item.__dict__.copy()
		del item_data["external_id"]
		Item.__init__(self, external_id=int(product_id), **item_data)
		self.product = product


@dataclass
class Webhook:
	webhook_label: str
	webhook_url: str
	webhook_action: str
	webhook_app_id_int: str
	webhook_id: int = None
	webhook_async: int = 1

	@classmethod
	def create_connector_webhook(cls, label: str, url: str, action: str = "sale"):
		return Webhook(
			webhook_label=label,
			webhook_url=url,
			webhook_action=action,
			webhook_app_id_int=SALE_WEBHOOK_APP_ID,
		)

	@classmethod
	def create_from_data(cls, data: dict):
		return Webhook(
			webhook_id=data["webhook_id"],
			webhook_label=data["webhook_label"],
			webhook_url=data["webhook_url"],
			webhook_action=data["webhook_action"],
			webhook_app_id_int=data["webhook_app_id_int"],
		)

	@property
	def data(self) -> dict:
		rv = self.__dict__.copy()
		try:
			del rv["webhook_id"]
		except KeyError:
			pass
		return rv


@dataclass
class InventoryInputData:
	stock_id: int


@dataclass
class InventoryInputDetailData:
	quantity: int
	product_id: int
	product_size: int

@dataclass
class Tax:
	tax_id: int
	tax_value: str
	tax_enabled: bool
	tax_name: str

	@classmethod
	def create_from_data(cls, data: dict):
		return Tax(
			tax_id=data["tax_id"],
			tax_value=data["tax_value"],
			tax_enabled=data["tax_enabled"],
			tax_name=data["tax_name"],
		)
