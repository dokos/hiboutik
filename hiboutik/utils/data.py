import pycountry

import frappe

def get_country_name(iso_code):
	alpha_3_data = pycountry.countries.get(alpha_3=iso_code)
	return frappe.db.get_value("Country", dict(code=alpha_3_data.alpha_2))

def get_country_iso_code(country):
	iso_code = frappe.db.get_value("Country", country, "code")
	return pycountry.countries.get(alpha_2=iso_code).alpha_3
