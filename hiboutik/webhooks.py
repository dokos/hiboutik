import json

import frappe
from frappe.utils.data import now_datetime

from hiboutik.hiboutik.api import HiboutikAPI, HiboutikAPIError

@frappe.whitelist(allow_guest=True)
def pos_invoice_webhook(*args, **kwargs):
	"""Receives a POS Invoice from Hiboutik"""

	data = frappe.parse_json(kwargs)

	return frappe.get_doc({
		"doctype": "Hiboutik Sales",
		"hiboutik_unique_sales_id": data.get("unique_sale_id"),
		"data": json.dumps(data, indent=2),
		"date": now_datetime()
	}).insert(ignore_permissions=True, ignore_if_duplicate=True)


@frappe.whitelist(allow_guest=True)
def pos_customer_webhook(*args, **kwargs):
	"""Receives a customer from Hiboutik"""

	data = frappe.parse_json(kwargs)
	return create_update_customer(data.get("customer_id"))


def create_update_customer(customer_id):
	hiboutik_settings = frappe.get_single("Hiboutik Settings")
	hiboutik_api = HiboutikAPI(hiboutik_settings)

	try:
		customers = hiboutik_api.get_customer(customer_id)
		for customer in customers:
			if not frappe.db.exists("Customer", dict(hiboutik_id=customer_id)):
				doc = frappe.new_doc("Customer")
			else:
				doc = frappe.get_doc("Customer", dict(hiboutik_id=customer_id))

			doc.hiboutik_id = customer.external_id
			doc.customer_name = customer.company if customer.company else f"{customer.first_name} {customer.last_name}"
			doc.customer_type = "Company" if customer.company else "Individual"
			doc.flags.ignore_permissions = True
			doc.save()

			try:
				create_update_contact(customer, doc.name)
			except Exception:
				pass

			for address in customer.addresses:
				create_update_address(address, doc.name)
	except HiboutikAPIError as e:
		if frappe.parse_json(e.args[0]).get("error") == "invalid_data":
			frappe.db.set_value("Customer", dict(hiboutik_id=customer_id), "hiboutik_id", None)

def create_update_contact(customer, link_name):
	contact = frappe.db.get_value("Contact", dict(email_id=customer.email))

	doc = frappe.get_doc("Contact", contact) if contact else frappe.new_doc("Contact")
	doc.first_name = customer.first_name
	doc.last_name = customer.last_name
	doc.add_email(customer.email, True)
	doc.add_phone(customer.phone, True)
	doc.append("links", {
		'link_doctype': 'Customer',
		'link_name': link_name
	})
	doc.flags.ignore_permissions = True
	doc.save()

def create_update_address(address, link_name):
	existing_address = frappe.db.get_value("Address", dict(hiboutik_id=address.external_id))

	doc = frappe.get_doc("Address", existing_address) if existing_address else frappe.new_doc("Address")
	doc.hiboutik_id = address.external_id
	doc.address_line1 = address.address
	doc.zip_code = address.zip_code
	doc.city = address.city
	doc.country = address.country
	doc.append("links", {
		'link_doctype': 'Customer',
		'link_name': link_name
	})
	doc.flags.ignore_permissions = True
	doc.save()
