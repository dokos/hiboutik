import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import cint, flt

from erpnext.utilities.product import get_price

from hiboutik.hiboutik.api import (
	HiboutikAPI,
	HiboutikAPIError,
	HiboutikConnector
)
from hiboutik.hiboutik.models import Item


@frappe.whitelist()
def sync_all_items():
	"""Synchronize all items where 'hiboutik_sync' checkbox is ticked"""

	items = frappe.db.get_list(
		"Item",
		filters={"sync_with_hiboutik": 1, "disabled": 0},
		pluck="item_code",
	)

	for item in items:
		res = _sync_item_to_hiboutik(item)
		if not res:
			frappe.msgprint(
				msg=_(f"Hiboutik synchronization error for item {0}.").format(item),
				title=_("Hiboutik Error"),
				raise_exception=HiboutikAPIError,
			)


@frappe.whitelist()
def sync_item(item):
	"""Given an Item, request sync to external POS"""
	return _sync_item_to_hiboutik(item)


def on_update(doc, method=None):
	"""
	Hook triggered when an Item is updated
	"""
	if frappe.db.get_single_value("Hiboutik Settings", "enable_sync"):
		return _sync_item_to_hiboutik(doc.name)


def _sync_item_to_hiboutik(item_code: str):
	from erpnext.stock.utils import get_latest_stock_qty

	try:
		item_doc = frappe.get_doc("Item", item_code)

		# Get POS Profile from Hiboutik Settings
		hiboutik_settings = frappe.get_single("Hiboutik Settings")
		pos_profile = frappe.get_doc("POS Profile", hiboutik_settings.pos_profile)

		hb_tax_id = get_hiboutik_tax_id(item_doc)

		# Get Price given the Price List configured
		price = get_price(
			item_doc.item_code,
			pos_profile.selling_price_list,
			None,  # Customer_group
			pos_profile.company,
			qty=1,
		)

		item = Item(
			code=item_doc.item_code,
			name=item_doc.item_name,
			external_id=item_doc.hiboutik_id,
			price=flt(price.get("price_list_rate") if price else 0.0),
			vat=hb_tax_id,
			is_stock_item=item_doc.is_stock_item,
		)

		# If stocked item, fill in quantity from warehouse
		if item.is_stock_item:
			item.stock_qty = get_latest_stock_qty(item.code, pos_profile.warehouse)

		hiboutik_api = HiboutikAPI(hiboutik_settings)
		connector = HiboutikConnector(hiboutik_api)

		updated_item = connector.sync(item)

		if item_doc.hiboutik_id != updated_item.external_id:
			frappe.db.set_value("Item", item_doc.name, "hiboutik_id", updated_item.external_id)

		return True
	except Exception:
		frappe.log_error(_("Hiboutik Items Synchronization Error"))


def get_hiboutik_tax_id(item: Document) -> int:
	"""Returns the Hiboutik's tax ID corresponding to the tax set for the Item."""
	hiboutik_vat_rates = frappe.get_all(
		"Hiboutik VAT Rates", fields=["hiboutik_rate", "hiboutik_id", "item_tax_template"]
	)

	item_tax_template = get_item_tax_template_name(item)

	for vat in hiboutik_vat_rates:
		if item_tax_template == vat.item_tax_template:
			return cint(vat.hiboutik_id)

	return [vat.hiboutik_id for vat in hiboutik_vat_rates if vat.hiboutik_rate == 0][0]


def get_item_tax_template_name(item: Document) -> str:
	"""Given an Item returns the first Tax Template found either at the Item level
	or at the Item Group level. Returns an empty string if no tax template has been set for this item."""

	selling_tax_templates = get_selling_tax_templates()

	def get_item_tax():
		try:
			return [
				tax.item_tax_template
				for tax in item.taxes
				if tax.item_tax_template in selling_tax_templates
			][0]
		except IndexError:
			return None

	def get_item_group_tax():
		try:
			return frappe.get_all(
				"Item Tax",
				filters={
					"parenttype": "Item Group",
					"parent": item.item_group,
					"item_tax_template": ("in", selling_tax_templates),
				},
				pluck="item_tax_template",
			)[0]
		except IndexError:
			return None

	for getter in [get_item_tax, get_item_group_tax]:
		itt_name = getter()
		if itt_name:
			return itt_name

	return ""


def get_selling_tax_templates():
	filters = {}

	if frappe.get_meta("Item Tax Template").has_field("applicable_for"):
		filters={"applicable_for": ("in", ("Sales", None))}

	return frappe.get_all(
		"Item Tax Template", filters=filters, pluck="name"
	)
