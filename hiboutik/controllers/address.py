from dataclasses import dataclass, fields

import frappe

from hiboutik.utils.data import get_country_iso_code
from hiboutik.hiboutik.api import HiboutikAPI, HiboutikAPIError

def on_update(doc, method):
	if frappe.db.get_single_value("Hiboutik Settings", "enable_sync"):
		create_update_hiboutik_address(doc)

def create_update_hiboutik_address(doc):
	customer_id = get_customer_id(doc)
	if not customer_id:
		return

	hiboutik_settings = frappe.get_single("Hiboutik Settings")
	hiboutik_api = HiboutikAPI(hiboutik_settings)

	if not doc.hiboutik_id:
		_create_hiboutik_address(doc, customer_id, hiboutik_api)

	address = _get_hiboutik_address(doc)
	try:
		_update_hiboutik_address(doc.hiboutik_id, address, hiboutik_api)
	except HiboutikAPIError as e:
		if frappe.parse_json(e.args[0]).get("error") == "not_found":
			_create_hiboutik_address(doc, customer_id, hiboutik_api)

def _create_hiboutik_address(doc, customer_id, hiboutik_api):
	response = hiboutik_api.post_customer_address(customer_id)
	doc.hiboutik_id = response.get("address_id")
	frappe.db.set_value("Address", doc.name, "hiboutik_id", response.get("address_id"))

def _update_hiboutik_address(id, address, hiboutik_api):
	for field in fields(address):
		hiboutik_api.put_customer_address(
			id=id,
			data={
				"address_attribute": field.name,
				"new_value": getattr(address, field.name)
			}
		)

def get_customer_id(doc):
	for link in doc.links:
		if link.link_doctype=="Customer":
			hiboutik_id = frappe.db.get_value("Customer", link.link_name, "hiboutik_id")
			if hiboutik_id:
				return hiboutik_id

def _get_hiboutik_address(doc):
	return HiboutikAddress(
		address=doc.address_line1,
		zip_code=doc.pincode,
		city=doc.city,
		state=doc.state,
		country=get_country_iso_code(doc.country),
		default=doc.is_primary_address,
	)


@dataclass
class HiboutikAddress:
	address: str
	zip_code: str
	city: str
	state: str
	country: str
	default: bool
