from dataclasses import dataclass, asdict

import frappe

from hiboutik.controllers.address import create_update_hiboutik_address
from hiboutik.hiboutik.api import HiboutikAPI, HiboutikAPIError

@frappe.whitelist()
def sync_all_customers():
	customer_type = frappe.db.get_single_value("Hiboutik Settings", "type_of_customers_to_sync")
	filters = {"disabled": 0}
	if customer_type != "All":
		filters.update({"customer_type": customer_type})
	customers = frappe.get_all("Customer", filters)

	for customer in customers:
		doc = frappe.get_doc("Customer", customer.name)
		_create_update_hiboutik_customer(doc)
		frappe.db.commit()

def on_update(doc, method):
	if frappe.db.get_single_value("Hiboutik Settings", "enable_sync"):
		_create_update_hiboutik_customer(doc)

def _create_update_hiboutik_customer(doc):
	contact = _get_primary_contact(doc.name)
	customer = _get_hiboutik_customer(doc, contact)
	hiboutik_settings = frappe.get_single("Hiboutik Settings")
	hiboutik_api = HiboutikAPI(hiboutik_settings)

	if doc.hiboutik_id:
		try:
			_update_hiboutik_customer(customer, hiboutik_api)
		except HiboutikAPIError as e:
			if frappe.parse_json(e.args[0]).get("error") == "not_found":
				_create_hiboutik_customer(customer, hiboutik_api, doc)
	else:
		_create_hiboutik_customer(customer, hiboutik_api, doc)

	update_customer_addresses(doc)

def _create_hiboutik_customer(customer, hiboutik_api, doc):
	try:
		response = hiboutik_api.post_customer(asdict(customer))
		frappe.db.set_value("Customer", doc.name, "hiboutik_id", response.get("customers_id"))
	except HiboutikAPIError:
		frappe.log_error("HiboutikAPIError")

def _update_hiboutik_customer(customer, hiboutik_api):
	try:
		hiboutik_api.put_customer(customer.customers_ref_ext, asdict(customer))
	except HiboutikAPIError:
		frappe.log_error("HiboutikAPIError")

def _get_hiboutik_customer(doc, contact):
	return HiboutikCustomer(
		customers_first_name=contact.first_name,
		customers_last_name=doc.customer_name if doc.customer_type == "Company" else contact.last_name,
		customers_company=doc.customer_name if doc.customer_type == "Company" else None,
		customers_email=contact.email,
		customers_phone_number=contact.phone,
		customers_ref_ext=doc.hiboutik_id,
	) if contact else HiboutikCustomer(
		customers_first_name=None,
		customers_last_name=doc.customer_name,
		customers_company=doc.customer_name if doc.customer_type == "Company" else None,
		customers_email=None,
		customers_phone_number=None,
		customers_ref_ext=doc.hiboutik_id,
	)

def _get_primary_contact(customer):
	contacts = frappe.db.sql("""
		select `tabContact`.name, `tabContact`.first_name, `tabContact`.last_name, `tabContact`.email_id,
			`tabContact`.phone
			from `tabContact`, `tabDynamic Link`
			where `tabContact`.name = `tabDynamic Link`.parent and `tabDynamic Link`.link_name = %(customer)s
			and `tabDynamic Link`.link_doctype = 'Customer'
			and `tabContact`.is_primary_contact = 1
		""", {
			"customer": customer
		}, as_dict=True)

	return contacts[0] if contacts else None

def update_customer_addresses(doc):
	for address in frappe.get_all("Dynamic Link", dict(link_doctype="Customer", link_name=doc.name, parenttype="Address"), pluck="parent"):
		address_doc = frappe.get_doc("Address", address)
		create_update_hiboutik_address(address_doc)

@dataclass
class HiboutikCustomer:
	customers_first_name: str
	customers_last_name: str
	customers_email: str
	customers_phone_number: str
	customers_ref_ext: str
	customers_company: str = None
	customers_country: str = None
