import json

import frappe
from frappe.utils import get_datetime

from hiboutik.hiboutik.api import HiboutikAPI


def check_closed_sales():
	hiboutik_api = HiboutikAPI(frappe.get_single("Hiboutik Settings"))

	closed_sales = hiboutik_api.get_closed_sales()
	for sales in closed_sales:
		if not frappe.db.exists("Hiboutik Sale", sales.get("unique_sale_id")):
			sales_list = hiboutik_api.get_sales(sales.get("sale_id"))
			for sale in sales_list:
				try:
					frappe.get_doc(
						{
							"doctype": "Hiboutik Sales",
							"hiboutik_unique_sales_id": sale.get("unique_sale_id"),
							"data": json.dumps(sale, indent=2),
							"date": get_datetime(sale.get("completed_at")),
						}
					).insert(ignore_permissions=True, ignore_if_duplicate=True)
				except Exception:
					frappe.log_error("Hiboutik Sales Integration Error")
