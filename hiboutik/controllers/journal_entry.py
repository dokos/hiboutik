import frappe


def on_submit(doc, method):
	if doc.get("hiboutik_till_movement"):
		frappe.db.set_value("Hiboutik Till Movement", doc.hiboutik_till_movement, "status", "Completed")