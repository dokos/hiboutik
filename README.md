# Dokos Hiboutik 

## :hammer_and_wrench: How the Dokos Hiboutik directory works

Hiboutik connector for Dokos and ERPNext.
This connector has been developed to synchronize the sales documents created in Hiboutik with Dokos or ERPNext.

Hiboutik is a web-based cash register software to track your sales, your stock and your customers.

### Functionalities :white_check_mark:

- Synchronisation of item details from Dokos/ERPNext to Hiboutik
- Synchronisation of individual customers from Dokos/ERPNext to Hiboutik
- Creation of Dokos/ERPNext sales invoices for each Hiboutik sale
- Stock balance synchronisation for each item

### Current limitations :white_check_mark:

- It is only possible to use a single default warehouse
- Customer loyalty points are not handled in Dokos

## :rocket: Dokos project 

Dokos is a 100% open-source management software that is based on ERPNext.

It is distributed under the GPLv3 license.

The 100% web architecture of Dokos allows you to use it in a public cloud as well as in a private cloud.

You can install it on your own servers.

The Dodock framework on which Dokos is installed also allows the installation of other applications. You can thus develop your own customizations, host them on your server and make them evolve in parallel with Dokos, without modifying its source code.

## :books: Use and documentation

#### Installation and use :construction:

##### 1. Documentation to set up Dokos Hiboutik : [Dokos Hiboutik documentation](https://doc.dokos.io/fr/integrations/hiboutik)

##### 2. Installation

This connector is compatible with Dokos.

Branches:
- Version 2: `master`
- Version 3: `v3.x.x`

To use it, you need to install Dokoson a server first.
Then you can get this application in you bench using the following command:

`bench get-app https://gitlab.com/dokos/hiboutik.git --branch <branchname>`

You can then choose to install it on your site.
For example, if you site is named demo.com:

`bench --site demo.com install-app hiboutik`

##### 3. Access to the Dokos community: [Forum](https://community.dokos.io/)

#### How to contribute :rocket:

**Description of the process and the branches to use**

1. I create a ticket with my proposal

- if it is for a new feature

- if a discussion is needed before making a Merge Request

2. I propose a Merge Request with my fixes/new features

3. The merge request must always be proposed on the develop branch and a retrofit request must be made in the merge request description

4. The merge request must contain a complete description and possibly screenshots

5. If a documentation is needed, it must be created on the wiki before the Merge Request is sent for validation

:point_right: Link to submit a ticket: **[Here](https://gitlab.com/dokos/dokos/-/issues)**

### :link: Useful links

- Detailed documentation: [doc.dokos.io](https://doc.dokos.io/fr/home)

- Community forum: [community.dokos.io](https://community.dokos.io/)

- Youtube channel: [Dokos Youtube](https://www.youtube.com/channel/UC2f3m8QANAVfKi2Pzw2fBlw)

- The Facebook page: [Dokos Facebook](https://www.facebook.com/dokos.io)

- The Linkedin page: [Dokos Linkedin](https://www.linkedin.com/company/dokos.io)

### :grey_question: Others informations

#### Website :card_index_dividers:

For details and documentation, see the website

[https://dokos.io](https://dokos.io)

#### Credits :pencil2:

The first version of this connector has been written by ioCraft.
Source code: https://github.com/io-craft-org/opossum

The developments are funded by the following third places:

- https://www.cooperativebaraka.fr/
- https://lacoroutine.org/
- MonsFabrica

#### License :page_facing_up:

This repository has been released under the [GNU-GPLv3](LICENSE).

#### Publisher :pushpin:

Dokos SAS
